# Menjalankan Prometheus di Docker #

docker run \
    -p 9090:9090 \
    -v absoulte/path/to/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
